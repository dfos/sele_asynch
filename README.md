# Asynchronous Communication System
# UART as Master-Slave Bus

## Description

In this project, there are 3 MCU's. 1 Master, 2 Slaves. In the PCB there are DIP switches that assign an address to the MCU:
### DIP SWITCHES Configuration
* 1111 for MASTER (0xF)
* 1110 for SLAVE 2 (0xE)
* 1101 for SLAVE 1 (0xD)

__Reset must be pressed for the address changes take effect__

The Master MCU controls both slaves. The protocol is explained in the **Protocol** section
In this example, the only slave function is to turn **on** or **off**  a LED.
For this, we used a MAX485 transceiver, which allows us to communicate through a long distance (up to 1Km).

## Protocol
### Frames 
Each frame is formed by a **Start Bit** followed by 8 bit **data** and a **9th bit** (which tells the Slaves if the frame contains an **Address** or not) and 1 Stop bit.

Before sending data to the Slaves, the Master has to send their address first. 
This is done by sending an address frame with the 9th bit set to 1. Only then the target Slave is 'activated' and ready to receive data. In this case, either a 0 (turn off led) or a 1 (turn on led).
After a Slave is selected, it waits for data in a loop until it detects an address frame. After that, the Slave enters an idle state which is only interrupted if an address frame containing its own address is detected.


## Setup:

1. ```git clone https://git.fe.up.pt/up201505033/asynch_t4_b06.git```
2. Compile main.cpp (we used platformIO)
3. Flash the MCU's (we used AVRDUDE for testing, but since the PCB does not have a USB port, you might want to search other flashing methods)


## Schematic

![Schematic](easyeda/Schematic_SELE-LAB2_Schematic_20191202194148.png  "Schematic")

### PCB

![PCB](easyeda/PCB.svg "PCB")

### EasyEDA project

[Link to EasyEDA project](https://easyeda.com/joaosantos97/test)
