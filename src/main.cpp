#include <Arduino.h>

#define MASTER_ADDR 15
#define SLAVE1_ADDR 13
#define SLAVE2_ADDR 14

uint8_t ADDR;
uint8_t previous_addr;
uint8_t data;
int button1 = 0;
int button2 = 0;

#define LED_PIN 13
#define ADDR3_PIN 11
#define ADDR2_PIN 10
#define ADDR1_PIN 9
#define ADDR0_PIN 8
#define BUT1_PIN 7
#define BUT2_PIN 6
#define WREN_PIN 2

#define BAUD 9600
#define FOSC 16000000 // Clock Speed
#define MYUBRR FOSC / 16 / BAUD - 1

void asynch9_init(unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H = (unsigned char)(ubrr >> 8);
	UBRR0L = (unsigned char)ubrr;
	/*Enable receiver and transmitter and 9 bit data transmission */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << UCSZ02);
	/* Set frame format: 9 bit data, 1stop bit */
	UCSR0C = (3 << UCSZ00);
}

void send_addr(uint8_t addr)
{
	/* Wait for empty transmit buffer */
	while (!(UCSR0A & (1 << UDRE0)))
		;
	/* Copy 9th bit to TXB8 */
	UCSR0B |= (1 << TXB80);
	/* Put data into buffer, sends the data */
	UDR0 = addr;
}

void send_data(uint8_t data)
{
	/* Wait for empty transmit buffer */
	while (!(UCSR0A & (1 << UDRE0)))
		;
	/* Disable TXB8 bit */
	UCSR0B &= ~(1 << TXB80);
	/* Put data into buffer, sends the data */
	UDR0 = data;
}

int get_data(uint8_t *data)
{
	unsigned char resh;

	/* Wait for data to be received */
	while (!(UCSR0A & (1 << RXC0)))
		;

	/* Get status and 9th bit, then data */
	/* from buffer */
	resh = UCSR0B;
	*data = UDR0;

	/* No error checking in this particular project */
	/* because we do not want to stop the slave microcontroller */
	/* If there's an error it simply ignores and does nothing */

	/* Filter the 9th bit and return it */
	return (resh >> 1) & 0x0001;
}

void setup()
{
	pinMode(ADDR0_PIN, INPUT_PULLUP);
	pinMode(ADDR1_PIN, INPUT_PULLUP);
	pinMode(ADDR2_PIN, INPUT_PULLUP);
	pinMode(ADDR3_PIN, INPUT_PULLUP);
	pinMode(BUT1_PIN, INPUT_PULLUP);
	pinMode(BUT2_PIN, INPUT_PULLUP);

	ADDR = (uint8_t)((digitalRead(ADDR3_PIN) << 3) | (digitalRead(ADDR2_PIN) << 2) | (digitalRead(ADDR1_PIN) << 1) | digitalRead(ADDR0_PIN));

	if (ADDR == MASTER_ADDR)
	{
		pinMode(WREN_PIN, OUTPUT);
		digitalWrite(WREN_PIN, HIGH);
	}
	else
	{
		pinMode(WREN_PIN, OUTPUT);
		pinMode(LED_PIN, OUTPUT);
		digitalWrite(WREN_PIN, LOW);
		digitalWrite(LED_PIN, LOW);
	}
	asynch9_init(MYUBRR);
}

void process_data(uint8_t data)
{
	if (data == 1)
		digitalWrite(LED_PIN, HIGH);

	else if (data == 0)
		digitalWrite(LED_PIN, LOW);
}

/* address: destination address || previous: previous address || data: data to send || button: button state */
void send(uint8_t address, uint8_t *previous, uint8_t data, int *button)
{
	if (*button == 1)
		*button = 0;
	else if (*button == 0)
		*button = 1;

	if (*previous != address)
		send_addr(address);
	send_data(data);

	*previous = address;
}
void loop()
{
	if (ADDR == MASTER_ADDR)
	{
		if (!digitalRead(BUT1_PIN) && button1 == 0)
			send(SLAVE1_ADDR, &previous_addr, 1, &button1);
		else if (digitalRead(BUT1_PIN) && button1 == 1)
			send(SLAVE1_ADDR, &previous_addr, 0, &button1);
		if (!digitalRead(BUT2_PIN) && button2 == 0)
			send(SLAVE2_ADDR, &previous_addr, 1, &button2);
		else if (digitalRead(BUT2_PIN) && button2 == 1)
			send(SLAVE2_ADDR, &previous_addr, 0, &button2);
	}
	else	//Slave
	{
		int type;
		UCSR0A |= (1 << MPCM0);
		type = get_data(&data);

		if (data == ADDR)
		{
			UCSR0A &= ~(1 << MPCM0);

			while (1)
			{
				type = get_data(&data);
				if (type == 1 && data != ADDR)
					break;
				process_data(data);
			}
		}
	}
}